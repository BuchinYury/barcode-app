package ru.n1sport.bll

import org.koin.dsl.module
import ru.n1sport.bll.stores.AppStore
import ru.n1sport.bll.stores.AppStoreState
import ru.n1sport.bll.usecases.DressmakerUseCases
import ru.n1sport.bll.usecases.MasterUseCases
import ru.n1sport.bll.usecases.MechanicUseCases
import ru.n1sport.bll.usecases.PassportUseCases


val useCasesModule = module {
    single { DressmakerUseCases(get()) }
    single { PassportUseCases(get()) }
    single { MechanicUseCases(get()) }
    single { MasterUseCases(get()) }
}

val appStoreModule = module {
    single { AppStoreState() }
    single {
        AppStore(
            get(),
            get(),
            get(),
            get(),
            get()
        )
    }
}

val gatewayModule = ru.n1sport.dal.gatewayModule