package ru.n1sport.bll.usecases

import ru.n1sport.dal.gateways.N1SportApi
import ru.n1sport.dal.models.request.Operation
import ru.n1sport.dal.models.request.PassportRequest
import ru.n1sport.dal.models.request.SendOperationsRequest

class PassportUseCases(
    private val n1SportApi: N1SportApi
) {
    fun getPassport(passportId: String, dressmakerId: String) =
        n1SportApi
            .getPassport(PassportRequest(
                passportId = passportId,
                dressmakerId = dressmakerId
            ))

    fun sendOperations(sendOperationsRequest: SendOperationsRequest) =
        n1SportApi
            .sendOperations(sendOperationsRequest)
}