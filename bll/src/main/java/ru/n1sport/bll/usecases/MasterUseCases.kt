package ru.n1sport.bll.usecases

import ru.n1sport.dal.gateways.N1SportApi
import ru.n1sport.dal.models.request.CallMasterRequest
import ru.n1sport.dal.models.request.CallMechanicRequest


class MasterUseCases(
    private val n1SportApi: N1SportApi
) {
    fun callMaster(dressmakerId: String) =
        n1SportApi
            .callMaster(CallMasterRequest(dressmakerId = dressmakerId))
}