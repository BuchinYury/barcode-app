package ru.n1sport.bll.usecases

import ru.n1sport.dal.gateways.N1SportApi
import ru.n1sport.dal.models.request.DressmakerRequest

class DressmakerUseCases(
    private val n1SportApi: N1SportApi
) {
    fun getDressmaker(dressmakerId: String) =
        n1SportApi
            .getDressmaker(DressmakerRequest(id = dressmakerId))
}