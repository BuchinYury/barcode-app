package ru.n1sport.bll.usecases

import ru.n1sport.dal.gateways.N1SportApi
import ru.n1sport.dal.models.request.CallMechanicRequest


class MechanicUseCases(
    private val n1SportApi: N1SportApi
) {
    fun callMechanic(dressmakerId: String) =
        n1SportApi
            .callMechanic(CallMechanicRequest(dressmakerId = dressmakerId))
}