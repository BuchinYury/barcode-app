package ru.n1sport.bll.stores


data class AppStoreState(
    val dressmaker: DressmakerState = DressmakerState(),
    val passport: PassportState = PassportState(),
    val isLoading: Boolean = false,
    val error: Throwable? = null
)

data class DressmakerState(
    val barcode: String? = null,
    val lastname: String? = null,
    val firstname: String? = null,
    val patronymic: String? = null,
    val imgUrl: String? = null
)

data class PassportState(
    val barcode: String? = null,
    val order: String? = null,
    val orderId: String? = null,
    val customer: String? = null,
    val product: String? = null,
    val option: String? = null,
    val design: String? = null,
    val designImgUrl: String? = null,
    val passportId: String? = null,
    val date: String? = null,
    val count: Int? = null,
    val operations: List<Operation> = emptyList()
)

data class Operation(
    val id: String,
    val sku: String,
    val name: String,
    val count: Int
)