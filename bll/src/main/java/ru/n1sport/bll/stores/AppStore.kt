package ru.n1sport.bll.stores

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import ru.n1sport.bll.stores.AppStoreAction.DressmakerBarcodeDetected
import ru.n1sport.bll.usecases.DressmakerUseCases
import ru.n1sport.bll.usecases.MasterUseCases
import ru.n1sport.bll.usecases.MechanicUseCases
import ru.n1sport.bll.usecases.PassportUseCases
import ru.n1sport.dal.models.request.SendOperationsRequest

class AppStore(
    initialState: AppStoreState,
    dressmakerUseCases: DressmakerUseCases,
    passportUseCases: PassportUseCases,
    mechanicUseCases: MechanicUseCases,
    masterUseCases: MasterUseCases
) {
    val state: Subject<AppStoreState> = BehaviorSubject.create()
    val actions: Subject<AppStoreAction> = PublishSubject.create()

    init {
        val dressmakerBarcodeDetected = actions.filter { it is DressmakerBarcodeDetected }
            .map { (it as DressmakerBarcodeDetected).barcode }
            .flatMap { barcode ->
                dressmakerUseCases.getDressmaker(barcode)
                    .toObservable()
                    .map { dressmakerResponse ->
                        { currentState: AppStoreState ->
                            val newDressmakerState = currentState.dressmaker.copy(
                                barcode = barcode,
                                lastname = dressmakerResponse.lastname,
                                firstname = dressmakerResponse.firstname,
                                patronymic = dressmakerResponse.patronymic,
                                imgUrl = dressmakerResponse.imgUrl
                            )
                            currentState.copy(
                                dressmaker = newDressmakerState,
                                passport = PassportState(),
                                isLoading = false,
                                error = null
                            )
                        }
                    }
                    .networkOperators()
            }
        val passportBarcodeDetected = actions.filter { it is AppStoreAction.PassportBarcodeDetected }
            .map { (it as AppStoreAction.PassportBarcodeDetected).barcode }
            .withLatestFrom(state) { passportId, state ->
                passportId to (state.dressmaker.barcode ?: "")
            }
            .flatMap { (passportId, dressmakerId) ->
                passportUseCases.getPassport(passportId, dressmakerId)
                    .toObservable()
                    .map { passportResponse ->
                        { currentState: AppStoreState ->
                            val newPassportState = currentState.passport.copy(
                                barcode = passportId,
                                order = passportResponse.order,
                                orderId = passportResponse.orderId,
                                customer = passportResponse.customer,
                                product = passportResponse.product,
                                option = passportResponse.option,
                                design = passportResponse.design,
                                designImgUrl = passportResponse.designImgUrl,
                                passportId = passportResponse.passpotrId,
                                date = passportResponse.date,
                                count = passportResponse.count,
                                operations = passportResponse.operations.map {
                                    Operation(
                                        id = it.id,
                                        sku = it.sku,
                                        name = it.name,
                                        count = it.count
                                    )
                                }
                            )
                            currentState.copy(
                                passport = newPassportState,
                                isLoading = false,
                                error = null
                            )
                        }
                    }
                    .networkOperators()
            }
        val errorNotificationReceived = actions.filter { it === AppStoreAction.ErrorNotificationReceived }
            .map {
                { currentState: AppStoreState ->
                    currentState.copy(error = null)
                }
            }
        val sendOperations = actions.filter { it is AppStoreAction.SendOperations }
            .map { (it as AppStoreAction.SendOperations).operations }
            .map {
                it.map {
                    ru.n1sport.dal.models.request.Operation(
                        it.id,
                        it.count
                    )
                }
            }
            .withLatestFrom(state) { operations, currentState ->
                SendOperationsRequest(
                    passportId = currentState.passport.barcode ?: "",
                    dressmakerId = currentState.dressmaker.barcode ?: "",
                    madeOperations = operations
                )
            }
            .flatMap {
                passportUseCases.sendOperations(it)
                    .andThen(
                        Observable.just { currentState: AppStoreState ->
                            currentState.copy(
                                passport = PassportState(),
                                isLoading = false,
                                error = null
                            )
                        }
                    )
                    .networkOperators()
            }
        val callMechanic = actions.filter { it === AppStoreAction.CallMechanic }
            .withLatestFrom(state) { _, currentState ->
                currentState.dressmaker.barcode
            }
            .flatMap {
                mechanicUseCases.callMechanic(it)
                    .andThen(
                        Observable.just { currentState: AppStoreState ->
                            currentState.copy(
                                isLoading = false,
                                error = null
                            )
                        }
                    )
                    .networkOperators()
            }
        val callMaster = actions.filter { it === AppStoreAction.CallMaster }
            .withLatestFrom(state) { _, currentState ->
                currentState.dressmaker.barcode
            }
            .flatMap {
                masterUseCases.callMaster(it)
                    .andThen(
                        Observable.just { currentState: AppStoreState ->
                            currentState.copy(
                                isLoading = false,
                                error = null
                            )
                        }
                    )
                    .networkOperators()
            }
        val exit = actions.filter { it === AppStoreAction.Exit }
            .map {
                { currentState: AppStoreState ->
                    currentState.copy(
                        dressmaker = DressmakerState(),
                        passport = PassportState()
                    )
                }
            }

        val stateReducers = listOf(
            dressmakerBarcodeDetected,
            passportBarcodeDetected,
            errorNotificationReceived,
            sendOperations,
            callMechanic,
            callMaster,
            exit
        )

        Observable.merge(stateReducers)
            .scan(initialState) { currentState, stateReducer -> stateReducer(currentState) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(state)
    }

    private fun Observable<(AppStoreState) -> AppStoreState>.networkOperators() =
        this.startWith { currentState: AppStoreState ->
            currentState.copy(
                isLoading = true,
                error = null
            )
        }
            .onErrorReturn {
                { currentState: AppStoreState ->
                    currentState.copy(
                        isLoading = false,
                        error = it
                    )
                }
            }
}

sealed class AppStoreAction {
    object ErrorNotificationReceived : AppStoreAction()
    object CallMechanic : AppStoreAction()
    object CallMaster : AppStoreAction()
    object Exit : AppStoreAction()
    class DressmakerBarcodeDetected(val barcode: String) : AppStoreAction()
    class PassportBarcodeDetected(val barcode: String) : AppStoreAction()
    class SendOperations(val operations: List<ru.n1sport.bll.models.Operation>) : AppStoreAction()
}
