package ru.n1sport.bll.models

class Operation (
    val id: String,
    val count: Int
)