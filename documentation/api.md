# N1SportApi

Api Android приложения

# Group Api

## Исполнитель [/futbitex/hs/futbitexservice/name]
### Получение информации об исполнителе по id [POST]
+ Request (application/json)
    + Attributes (object)
        + id: 000000002 (string, required) - id исполнителя

    + Body

            {
                "id": "000000002"
            }

    + Schema

            {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "description": "id исполнителя"
                    }
                }
            }

+ Response 200 (application/json)

    + Attributes (object)
        + lastname: Максина (string, required) - фамилия исполнителя
        + firstname: Любовь (string, required) - имя исполнителя
        + patronymic: Александровна (string, required) - отчетсво исполнителя
        + img_url: https://loremflickr.com/300/500 (string, required) - url для получения изображения исполнителя

    + Body

            {
                "lastname": "Максина",
                "firstname": "Любовь",
                "patronymic": "Александровна",
                "img_url": "http://192.168.1.106:8080/Members/000000035.jpg"
            }

    + Schema

            {
                "type": "object",
                "properties": {
                    "lastname": {
                        "type": "string",
                        "required": true,
                        "description": "фамилия исполнителя"
                    },
                    "firstname": {
                        "type": "string",
                        "required": true,
                        "description": "имя исполнителя"
                    },
                    "patronymic": {
                        "type": "string",
                        "required": true,
                        "description": "отчетсво исполнителя"
                    },
                    "img_url": {
                        "type": "string",
                        "required": true,
                        "description": "url для получения изображения исполнителя"
                    }
                }
            }


## Паспорт изделия [/futbitex/hs/futbitexservice/passport]
### Получение паспорта [POST]
+ Request (application/json)
    + Attributes (object)
        + id_pass: U3742818054005005 (string, required) - id паспорта
        + id_empl: U3742818054005005 (string, required) - id исполнителя

    + Body

            {
                "id_pass": "U3742818054005005",
                "id_empl": "000000002"
            }

    + Schema

            {
                "type": "object",
                "properties": {
                    "id_passd": {
                        "type": "string",
                        "description": "id паспорта"
                    },
                    "id_empl": {
                        "type": "string",
                        "description": "id исполнителя"
                    }
                }
            }        

+ Response 200 (application/json)
    + Attributes (object)
        + order_id: `00ФТ-004516` (string, required) - id заказа
        + order: `Заказ покупателя 4516 от 27.12.2018` (string, required) - название заказа
        + customer: `ООО «Презент» (г. Владивосток)` (string, required) - заказчик
        + product: `Толстовка втачной рукав. Модель 1 (Футер трехнитка)` (string, required) - изделие
        + option: `54-188` (string, required) - размер изделия
        + design: ` (string, required) - описание дизайна
        + id_passport: `00ФТ-037428 (string, required) - id паспорта
        + id_date: `29.12.2018 (string, required) - дата паспорта
        + design_img_url: https://loremflickr.com/300/500 (string, required) - url для получения изображения дизайна изделия
        + count: 5 (number, required) - количество изделий (целое число)
        + operations (array, required) - массив с операциями паспорта
            + operation (object)
                + id: М (string, required) - id операции
                + sku: М (string, required) - артикул операции
                + name: Маркировка (string, required) - название операции
                + count: 3 (number, required) - количество несделанных операции (целое число)

    + Body

            {
              "order_id": "00ФТ-004516",
              "order": "Заказ покупателя 4516 от 27.12.2018 ",
              "customer": "ООО «Презент» (г. Владивосток)",
              "product": "Толстовка втачной рукав. Модель 1 (Футер трехнитка)",
              "option": "54-188",
              "design": "",
              "id_passport": "00ФТ-037428",
              "id_date": "29.12.2018",
              "design_img_url": "http://192.168.1.106:8080/Designs/Flower%20of%20Fire%280029129012019%29.png",
              "count": 5,
              "operations": [
                {
                  "id": "К",
                  "sku": "К",
                  "name": "Разобрать крой Разобрать крой Разобрать крой Разобрать крой Разобрать крой Разобрать крой Разобрать крой Разобрать крой",
                  "count": 5
                },
                {
                  "id": "ПТ",
                  "sku": "ПТ",
                  "name": "Печать трансфера",
                  "count": 5
                },
                {
                  "id": "9/18",
                  "sku": "9/18",
                  "name": "Намеловка",
                  "count": 5
                },
                {
                  "id": "7/15",
                  "sku": "7/15",
                  "name": "Отстрочить декоративную строчку",
                  "count": 5
                },
                {
                  "id": "7/16",
                  "sku": "7/16",
                  "name": "Отстрочить плечевые швы",
                  "count": 5
                },
                {
                  "id": "9/1",
                  "sku": "9/1",
                  "name": "Поставить закрепки",
                  "count": 5
                },
                {
                  "id": "1/33",
                  "sku": "1/33",
                  "name": "Настрочить обтачку",
                  "count": 5
                },
                {
                  "id": "4/38",
                  "sku": "4/38",
                  "name": "Обметать обтачку",
                  "count": 5
                },
                {
                  "id": "1",
                  "sku": "1",
                  "name": "Стачать плечевые швы",
                  "count": 5
                },
                {
                  "id": "2а",
                  "sku": "2а",
                  "name": "Стачать подвяз",
                  "count": 5
                },
                {
                  "id": "2",
                  "sku": "2",
                  "name": "Втачать подвяз",
                  "count": 5
                },
                {
                  "id": "4",
                  "sku": "4",
                  "name": "Втачать рукава",
                  "count": 5
                },
                {
                  "id": "5",
                  "sku": "5",
                  "name": "Стачать рук.-бок. срезы",
                  "count": 5
                },
                {
                  "id": "4/25",
                  "sku": "4/25",
                  "name": "Стачать манжеты и притачать к изделию",
                  "count": 5
                },
                {
                  "id": "1/26",
                  "sku": "1/26",
                  "name": "Стачать пояс с изделием",
                  "count": 5
                },
                {
                  "id": "23",
                  "sku": "23",
                  "name": "ОТК",
                  "count": 5
                },
                {
                  "id": "24",
                  "sku": "24",
                  "name": "ВТО",
                  "count": 5
                },
                {
                  "id": "24а",
                  "sku": "24а",
                  "name": "Упаковка",
                  "count": 5
                },
                {
                  "id": "СК",
                  "sku": "СК",
                  "name": "Сканирование",
                  "count": 5
                },
                {
                  "id": "М",
                  "sku": "М",
                  "name": "Маркировка",
                  "count": 5
                },
                {
                  "id": "10а",
                  "sku": "10а",
                  "name": "Пришить бирку",
                  "count": 5
                }
              ]
            }

    + Schema

            {
                "type": "object",
                "required": true,
                "properties": {
                    "order_id": {
                        "type": "string",
                        "required": true,
                        "description": "id заказа"
                    },
                    "order": {
                        "type": "string",
                        "required": true,
                        "description": "название заказа"
                    },
                    "customer": {
                        "type": "string",
                        "required": true,
                        "description": "заказчик"
                    },
                    "product": {
                        "type": "string",
                        "required": true,
                        "description": "изделие"
                    },
                    "option": {
                        "type": "string",
                        "required": true,
                        "description": "?"
                    },
                    "design": {
                        "type": "string",
                        "required": true,
                        "description": "?"
                    },
                    "id_passport": {
                        "type": "string",
                        "required": true,
                        "description": "id паспорта"
                    },
                    "id_date": {
                        "type": "string",
                        "required": true,
                        "description": "дата паспорта"
                    },
                    "design_img_url": {
                        "type": "string",
                        "description": "url для получения изображения дизайна изделия"
                    },
                    "count": {
                        "type": "number",
                        "required": true,
                        "description": "количество изделий (целое число)"
                    },
                    "operations": {
                        "type": "array",
                        "required": true,,
                        "description": "массив с операциями паспорта"
                        "items": {
                            "type": "object",
                            "properties": {
                                "id": {
                                    "type": "string",
                                    "required": true,
                                    "description": "id операции"
                                },
                                "name": {
                                    "type": "string",
                                    "required": true,
                                    "description": "название операции"
                                },
                                "count": {
                                    "type": "number",
                                    "required": true,
                                    "description": "количество несделанных операции (целое число)"
                                }
                            }
                        }
                    },
                }
            }

### Отправка сделанных операций [PUT]
+ Request (application/json)
    + Attributes (object)
        + id_pass: U3742818054005005 (string, required) - id паспорта
        + id_empl: 000000002 (string, required) - id сотрудника
        + operations (array, required) - массив с выполнеными операциями паспорта
            + operation (object)
                + id: М (string, required) - id операции
                + count: 3 (number, required) - количество выполненых операции (целое число)

    + Body

            {
                "id_pass": "U3742818054005005",
                "id_empl": "000000002",
                "operations": [
                    {
                        "id": "К",
                        "count": 5
                    },
                    {
                        "id": "10а",
                        "count": 5
                    }
                ]
            }

    + Schema

            {
                "type": "object",
                "required": true,
                "properties": {
                    "id_pass": {
                        "type": "string",
                        "required": true,
                        "description": "id паспорта"
                    },
                    "id_empl": {
                        "type": "string",
                        "required": true,
                        "description": "id сотрудника"
                    },
                    "operations": {
                        "type": "array",
                        "required": true,,
                        "description": "массив с выполнеными операциями паспорта"
                        "items": {
                            "type": "object",
                            "properties": {
                                "id": {
                                    "type": "string",
                                    "required": true,
                                    "description": "id операции"
                                },
                                "count": {
                                    "type": "number",
                                    "required": true,
                                    "description": "количество несделанных операции (целое число)"
                                }
                            }
                        }
                    },
                }
            }

+ Response 200

## Механик [/futbitex/hs/futbitexservice/call_mechanic]
### Вызов механика [POST]
+ Request (application/json)
    + Attributes (object)
        + id_empl: 000000002 (string, required) - id исполнителя

    + Body

            {
                "id_empl": "000000002"
            }

    + Schema

            {
                "type": "object",
                "properties": {
                    "id_empl": {
                        "type": "string",
                        "description": "id исполнителя"
                    }
                }
            }

+ Response 200 (application/json)

## Мастер [/futbitex/hs/futbitexservice/call_master]
### Вызов мастера [POST]
+ Request (application/json)
    + Attributes (object)
        + id_empl: 000000002 (string, required) - id исполнителя

    + Body

            {
                "id_empl": "000000002"
            }

    + Schema

            {
                "type": "object",
                "properties": {
                    "id_empl": {
                        "type": "string",
                        "description": "id исполнителя"
                    }
                }
            }

+ Response 200 (application/json)
