# Документация

## Название приложения/Название в тулбаре
Можно поменять в файде /app/build.gradle - параметр "app_name"

[Ссылка в репозитории](https://git.n1sport.ru/projects/AN/repos/android_app_tablet/browse/app/build.gradle#16)

## Адрес сервера
Можно поменять в файде /dal/build.gradle - параметр "DEFAULT_SERVER_URL"

[Ссылка в репозитории](https://git.n1sport.ru/projects/AN/repos/android_app_tablet/browse/dal/build.gradle#15)

## URL запросов
Можно поменять в файде /dal/src/main/java/ru/n1sport/dal/gateways/N1SportApi.kt

[Ссылка в репозитории](https://git.n1sport.ru/projects/AN/repos/android_app_tablet/browse/dal/src/main/java/ru/n1sport/dal/gateways/N1SportApi.kt)

## Закоментированный код вывода информации о паспорте
Находиться в файде /app/src/main/java/ru/n1sport/barcodeapp/features/passport/PassportFragment.kt

[Ссылка в репозитории](https://git.n1sport.ru/projects/AN/repos/android_app_tablet/browse/app/src/main/java/ru/n1sport/barcodeapp/features/passport/PassportFragment.kt#275)

## Вернуть не статический показ ошибки
Находиться в файде /app/src/main/java/ru/n1sport/barcodeapp/features/main/MainActivity.kt

[Закоментировать](https://git.n1sport.ru/projects/AN/repos/android_app_tablet/browse/app/src/main/java/ru/n1sport/barcodeapp/features/main/MainActivity.kt#90)

[Раскоментировать](https://git.n1sport.ru/projects/AN/repos/android_app_tablet/browse/app/src/main/java/ru/n1sport/barcodeapp/features/main/MainActivity.kt#91)

## Вспышка
Находиться в файде /app/src/main/java/ru/n1sport/barcodeapp/features/barcodescanner/BarcodeScannerFragment.kt

[Ссылка в репозитории](https://git.n1sport.ru/projects/AN/repos/android_app_tablet/browse/app/src/main/java/ru/n1sport/barcodeapp/features/barcodescanner/BarcodeScannerFragment.kt#130)
