package ru.n1sport.barcodeapp.common

import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import io.fotoapparat.Fotoapparat
import io.fotoapparat.preview.FrameProcessor
import io.fotoapparat.view.CameraRenderer
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.n1sport.barcodeapp.BuildConfig
import ru.n1sport.barcodeapp.R
import ru.n1sport.barcodeapp.features.barcodescanner.BarcodeScannerViewModel
import ru.n1sport.barcodeapp.features.barcodescanner.BarcodeSource
import ru.n1sport.barcodeapp.features.main.MainActivity
import ru.n1sport.barcodeapp.features.main.MainActivityViewModel
import ru.n1sport.barcodeapp.features.passport.DressmakerState
import ru.n1sport.barcodeapp.features.passport.OperationState
import ru.n1sport.barcodeapp.features.passport.OperationsAdapter
import ru.n1sport.barcodeapp.features.passport.PassportViewModel
import ru.n1sport.barcodeapp.features.passport.PassportViewState
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator


val appModule = module {
    single<SharedPreferences> {
        androidContext().getSharedPreferences(BuildConfig.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE)
    }
}

val navigationModule = module {
    single<Cicerone<Router>> {
        Cicerone.create()
    }
    single<Router> {
        val cicerone: Cicerone<Router> = get()
        cicerone.router
    }
    single<NavigatorHolder> {
        val cicerone: Cicerone<Router> = get()
        cicerone.navigatorHolder
    }

    factory<Navigator> { (activity: MainActivity) ->
        SupportAppNavigator(activity, R.id.framelayout_main_fragment_container)
    }
}

val mainActivityModule = module {
    viewModel {
        MainActivityViewModel(
            appStore = get()
        )
    }
}

val passportModule = module {
    single {
        val lastname = androidContext().resources.getString(R.string.lastname)
        val firstname = androidContext().resources.getString(R.string.firstname)
        val patronymic = androidContext().resources.getString(R.string.patronymic)

        PassportViewState(
            dressmaker = DressmakerState(
                lastname = lastname,
                firstname = firstname,
                patronymic = patronymic
            )
        )
    }

    factory { (operationUpdate: (OperationState) -> Unit) ->
        OperationsAdapter(operationUpdate)
    }

    viewModel {
        PassportViewModel(
            get(),
            get(),
            get()
        )
    }
}

val barcodeScannerModule = module {
    factory { (context: Context, renderer: CameraRenderer, frameProcessor: FrameProcessor) ->
        Fotoapparat
            .with(context)
            .into(renderer)
            .frameProcessor(frameProcessor)
            .cameraErrorCallback {
                Toast.makeText(context, it.toString(), Toast.LENGTH_LONG).show()
            }
            .build()
    }

    viewModel { (barcodeSource: BarcodeSource) ->
        BarcodeScannerViewModel(
            barcodeSource,
            get(),
            get()
        )
    }
}