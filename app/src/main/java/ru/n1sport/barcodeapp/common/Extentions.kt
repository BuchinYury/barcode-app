package ru.n1sport.barcodeapp.common

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.TypedValue
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.google.zxing.BinaryBitmap
import com.google.zxing.MultiFormatReader
import com.google.zxing.RGBLuminanceSource
import com.google.zxing.common.HybridBinarizer
import retrofit2.HttpException
import ru.n1sport.barcodeapp.R
import java.net.SocketTimeoutException
import java.net.UnknownHostException

fun <T> unsafeLazy(initializer: () -> T) = lazy(LazyThreadSafetyMode.NONE, initializer)

fun rotateBitmap(source: Bitmap, angle: Float): Bitmap {
    val matrix = Matrix()
    matrix.postRotate(angle)
    return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
}

fun decodeBitmap(bitmap: Bitmap): String? {
    val width = bitmap.width
    val height = bitmap.height
    val pixels = IntArray(width * height)
    bitmap.getPixels(pixels, 0, width, 0, 0, width, height)

    val source = RGBLuminanceSource(width, height, pixels)
    val binaryBitmap = BinaryBitmap(HybridBinarizer(source))

    val reader = MultiFormatReader()
    val result = try {
        reader.decode(binaryBitmap)
    } catch (_: Throwable) {
        null
    }
    return result?.text
}

inline fun Context.alert(builder: AlertDialog.Builder.() -> Unit): AlertDialog =
    AlertDialog.Builder(this).apply {
        builder()
    }
        .create()

fun AlertDialog.changeButtons(textSize: Float = 22f) {
    this.getButton(AlertDialog.BUTTON_POSITIVE)?.apply {
        setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        setBackgroundColor(ResourcesCompat.getColor(resources, R.color.malachite, null))
        setTextColor(ContextCompat.getColor(context, android.R.color.white))
    }
    this.getButton(AlertDialog.BUTTON_NEGATIVE)?.apply {
        setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        setBackgroundColor(ResourcesCompat.getColor(resources, R.color.aluminium, null))
        setTextColor(ContextCompat.getColor(context, android.R.color.white))
        setPadding(16, 0, 16, 0)

        (layoutParams as? LinearLayout.LayoutParams)?.marginEnd = 16
    }
}

fun Context.mapErrorToMessage(error: Throwable): String = when (error) {
    is UnknownHostException -> getString(R.string.error_unknown_host_exception_msg)
    is SocketTimeoutException -> getString(R.string.error_socket_timeout_exception_msg)
    is HttpException -> {
        getString(R.string.main_alert_error_message) +
            try {
                val errorMsg = error.response()?.errorBody()?.string()
                if (errorMsg.isNullOrEmpty()) ""
                else "\n $errorMsg"
            } catch (_: Throwable) {
                ""
            }
    }
    else -> "$error"
}

fun Context.vibrate(milliseconds: Long = 300) {
    (getSystemService(Context.VIBRATOR_SERVICE) as? Vibrator)?.apply {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrate(VibrationEffect.createOneShot(milliseconds, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            vibrate(milliseconds)
        }
    }
}

fun View.setVisibility(isVisible: Boolean) {
    this.visibility =
        if (isVisible) View.VISIBLE
        else View.GONE
}