package ru.n1sport.barcodeapp.common

import ru.n1sport.barcodeapp.features.barcodescanner.BarcodeScannerFragment
import ru.n1sport.barcodeapp.features.barcodescanner.BarcodeSource
import ru.n1sport.barcodeapp.features.passport.PassportFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object PassportScreen : SupportAppScreen() {
    override fun getFragment() = PassportFragment()
}

object DressmakerBarcodeScannerScreen : SupportAppScreen() {
    override fun getFragment() = BarcodeScannerFragment.getInstance(BarcodeSource.DressmakerBarcode)
}

object PassportBarcodeScannerScreen : SupportAppScreen() {
    override fun getFragment() = BarcodeScannerFragment.getInstance(BarcodeSource.PassportBarcode)
}
