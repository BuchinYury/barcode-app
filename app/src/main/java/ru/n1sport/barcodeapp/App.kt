package ru.n1sport.barcodeapp

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import ru.n1sport.barcodeapp.common.*
import ru.n1sport.bll.appStoreModule
import ru.n1sport.bll.gatewayModule
import ru.n1sport.bll.useCasesModule

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(
                listOf(
                    appModule,
                    gatewayModule,
                    useCasesModule,
                    appStoreModule,
                    navigationModule,
                    mainActivityModule,
                    passportModule,
                    barcodeScannerModule
                )
            )
        }
    }
}