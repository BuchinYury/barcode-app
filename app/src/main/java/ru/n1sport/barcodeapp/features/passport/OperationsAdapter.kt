package ru.n1sport.barcodeapp.features.passport

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_operation.view.*
import ru.n1sport.barcodeapp.R
import ru.n1sport.barcodeapp.common.vibrate


class OperationsAdapter(
    private val operationUpdate: (OperationState) -> Unit
) : RecyclerView.Adapter<OperationHolder>() {
    var operations = emptyList<OperationState>()

    override fun getItemCount(): Int = operations.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OperationHolder {
        val headerView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_operation, parent, false)
        return OperationHolder(
            operationUpdate,
            headerView
        )
    }

    override fun onBindViewHolder(operationHolder: OperationHolder, position: Int) {
        val operation = operations[position]
        operationHolder.bind(operation)
    }
}

class OperationHolder(
    private val operationUpdate: (OperationState) -> Unit,
    view: View
) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun bind(operationState: OperationState) {
        handleSku(operationState)
        handleOperationName(operationState)
        handleOperationCount(operationState)
        handleOperationAdd(operationState)
    }

    @SuppressLint("SetTextI18n")
    private fun handleSku(operationState: OperationState) {
        with(itemView) {
            operation_id.text = operationState.sku
        }
    }

    @SuppressLint("SetTextI18n")
    private fun handleOperationName(operationState: OperationState) {
        with(itemView) {
            operation.text = operationState.name
        }
    }

    @SuppressLint("SetTextI18n")
    private fun handleOperationCount(operationState: OperationState) {
        with(itemView) {
            operation_count.text = "${operationState.madeCount} из ${operationState.count}"

            button_decrement_made_count.isEnabled = !operationState.isAdded && operationState.count > 0
            button_increment_made_count.isEnabled = !operationState.isAdded && operationState.count > 0

            button_decrement_made_count.setOnClickListener {
                val newMadeCount = operationState.madeCount - 1

                if (newMadeCount >= 0 && !operationState.isAdded) {
                    itemView.context.vibrate()

                    val newOperationState = operationState.copy(
                        madeCount = newMadeCount
                    )
                    operationUpdate(newOperationState)
                }
            }

            button_increment_made_count.setOnClickListener {
                val newMadeCount = operationState.madeCount + 1

                if (newMadeCount <= operationState.count && !operationState.isAdded) {
                    itemView.context.vibrate()

                    val newOperationState = operationState.copy(
                        madeCount = newMadeCount
                    )
                    operationUpdate(newOperationState)
                }
            }

            button_decrement_made_count.setOnLongClickListener {
                val newMadeCount = (operationState.madeCount - 10).let {
                    if (it <= 0) 0
                    else it
                }

                if (newMadeCount <= operationState.count && !operationState.isAdded) {
                    itemView.context.vibrate()

                    val newOperationState = operationState.copy(
                        madeCount = newMadeCount
                    )
                    operationUpdate(newOperationState)

                    true
                } else {
                    false
                }
            }

            button_increment_made_count.setOnLongClickListener {
                val newMadeCount = (operationState.madeCount + 10).let {
                    if (it >= operationState.count) operationState.count
                    else it
                }

                if (newMadeCount <= operationState.count && !operationState.isAdded) {
                    itemView.context.vibrate()

                    val newOperationState = operationState.copy(
                        madeCount = newMadeCount
                    )
                    operationUpdate(newOperationState)

                    true
                } else {
                    false
                }
            }
        }
    }

    @SuppressLint("ResourceAsColor")
    private fun handleOperationAdd(operationState: OperationState) {
        with(itemView) {
            image_view_add.setImageResource(
                if (operationState.isAdded) {
                    R.drawable.ic_done_trout_24dp
                } else {
                    R.drawable.ic_add_trout_24dp
                }
            )

            image_view_add.setOnClickListener {
                if (operationState.madeCount > 0) {
                    itemView.context.vibrate()

                    val newOperationState = operationState.copy(
                        isAdded = !operationState.isAdded
                    )
                    operationUpdate(newOperationState)
                }
            }

            if (operationState.isAdded) {
                setBackgroundColor(ResourcesCompat.getColor(resources, R.color.malachite, null))
            } else {
                setBackgroundColor(Color.WHITE)
            }
        }
    }
}

class OperationStateDiffUtilCallback(
    private val oldList: List<OperationState>,
    private val newList: List<OperationState>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldOperationState = oldList[oldItemPosition]
        val newOperationState = newList[newItemPosition]
        return oldOperationState.id == newOperationState.id &&
            oldOperationState.name == newOperationState.name
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldOperationState = oldList[oldItemPosition]
        val newOperationState = newList[newItemPosition]
        return oldOperationState.madeCount == newOperationState.madeCount &&
            oldOperationState.count == newOperationState.count &&
            oldOperationState.isAdded == newOperationState.isAdded
    }
}