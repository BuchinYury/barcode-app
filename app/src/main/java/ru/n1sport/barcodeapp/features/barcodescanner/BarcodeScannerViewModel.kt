package ru.n1sport.barcodeapp.features.barcodescanner

import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import ru.n1sport.barcodeapp.features.main.MainActivityViewState
import ru.n1sport.bll.stores.AppStore
import ru.n1sport.bll.stores.AppStoreAction
import ru.terrakok.cicerone.Router


sealed class BarcodeScannerIntention {
    object NavigateToBackClicked : BarcodeScannerIntention()
    class BarcodeDetected(val barcode: String) : BarcodeScannerIntention()
}

sealed class BarcodeScannerSideEffect {
    object NavigateToBack : BarcodeScannerSideEffect()
}


class BarcodeScannerViewModel(
    barcodeSource: BarcodeSource,
    router: Router,
    appStore: AppStore
) : ViewModel() {
    val intentions: Subject<BarcodeScannerIntention> = PublishSubject.create()
    val sideEffects: Subject<BarcodeScannerSideEffect> = PublishSubject.create()
    val viewState: Subject<BarcodeScannerViewState> = BehaviorSubject.create()

    private val disposeBag = CompositeDisposable()

    init {
        when (barcodeSource) {
            BarcodeSource.DressmakerBarcode ->
                viewState.onNext(BarcodeScannerViewState("Сканирование сотрудника"))
            BarcodeSource.PassportBarcode ->
                viewState.onNext(BarcodeScannerViewState("Сканирование паспорта"))
        }
    }

    init {
        intentions.filter { it is BarcodeScannerIntention.BarcodeDetected }
            .map { (it as BarcodeScannerIntention.BarcodeDetected).barcode }
            .map {
                when (barcodeSource) {
                    BarcodeSource.DressmakerBarcode -> AppStoreAction.DressmakerBarcodeDetected(it)
                    BarcodeSource.PassportBarcode -> AppStoreAction.PassportBarcodeDetected(it)
                }
            }
            .doAfterNext { sideEffects.onNext(BarcodeScannerSideEffect.NavigateToBack) }
            .subscribe(appStore.actions)
    }

    init {
        intentions.filter { it === BarcodeScannerIntention.NavigateToBackClicked }
            .map { BarcodeScannerSideEffect.NavigateToBack }
            .subscribe(sideEffects)
    }

    init {
        sideEffects.filter { it === BarcodeScannerSideEffect.NavigateToBack }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                router.exit()
            }
            .addTo(disposeBag)
    }

    override fun onCleared() {
        super.onCleared()
        disposeBag.clear()
    }
}

data class BarcodeScannerViewState(
    val inviteText: String
)
