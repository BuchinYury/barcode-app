package ru.n1sport.barcodeapp.features.main

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import ru.n1sport.barcodeapp.R
import ru.n1sport.barcodeapp.common.PassportScreen
import ru.n1sport.barcodeapp.common.alert
import ru.n1sport.barcodeapp.common.changeButtons
import ru.n1sport.barcodeapp.common.mapErrorToMessage
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router


class MainActivity : AppCompatActivity() {

    // region injects
    private val router: Router by inject()
    private val navigatorHolder: NavigatorHolder by inject()
    private val navigator: Navigator by inject { parametersOf(this@MainActivity) }
    private val viewModel: MainActivityViewModel by viewModel()
    // endregion

    // region fields
    private val disposeBag = CompositeDisposable()
    // endregion

    // region lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        handleLoading()
        handleError()

        router.newRootScreen(PassportScreen)
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposeBag.clear()
    }
    // endregion

    // region handlers
    private fun handleLoading() {
        viewModel.viewState
            .map { it.isLoading }
            .subscribe {
                if (it)
                    progressbar_main_loading?.visibility = View.VISIBLE
                else
                    progressbar_main_loading?.visibility = View.GONE
            }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.isLoading }
            .subscribe {
                if (it)
                    framelayout_main_fragment_container?.visibility = View.GONE
                else
                    framelayout_main_fragment_container?.visibility = View.VISIBLE
            }
            .addTo(disposeBag)
    }

    private fun handleError() {
        viewModel.viewState
            .filter { it.error != null }
            .subscribe {
                val error = it.error!!
                alert {
                    setTitle(R.string.main_alert_error_title)
                    setMessage(mapErrorToMessage(error))

                    setPositiveButton("Ок") { _, _ ->
                        viewModel.intentions.onNext(MainActivityIntention.ErrorNotificationReceived)
                    }
                    setOnCancelListener {

                    }
                }
                    .apply {
                        show()
                    }

            }
            .addTo(disposeBag)
    }
    // endregion
}
