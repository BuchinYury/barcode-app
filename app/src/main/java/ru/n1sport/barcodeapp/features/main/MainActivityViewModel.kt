package ru.n1sport.barcodeapp.features.main

import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import ru.n1sport.bll.stores.AppStore
import ru.n1sport.bll.stores.AppStoreAction


sealed class MainActivityIntention {
    object ErrorNotificationReceived : MainActivityIntention()
}

class MainActivityViewModel(
    initialState: MainActivityViewState = MainActivityViewState(),
    appStore: AppStore
) : ViewModel() {
    val viewState: Subject<MainActivityViewState> = BehaviorSubject.create()
    val intentions: Subject<MainActivityIntention> = PublishSubject.create()

    init {
        val appStoreState = appStore.state
            .map { appStoreState ->
                { currentState: MainActivityViewState ->
                    currentState.copy(
                        isLoading = appStoreState.isLoading,
                        error = appStoreState.error
                    )
                }
            }

        val stateReducers = listOf(appStoreState)

        Observable.merge(stateReducers)
            .scan(initialState) { currentState, stateReducer -> stateReducer(currentState) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(viewState)
    }

    init {
        intentions.filter { it === MainActivityIntention.ErrorNotificationReceived }
            .map { AppStoreAction.ErrorNotificationReceived }
            .subscribe(appStore.actions)
    }
}

data class MainActivityViewState(
    val isLoading: Boolean = false,
    val error: Throwable? = null
)