package ru.n1sport.barcodeapp.features.passport

import android.Manifest
import android.os.Bundle
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.jakewharton.rxbinding3.view.clicks
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_passport.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import ru.n1sport.barcodeapp.R
import ru.n1sport.barcodeapp.common.alert
import ru.n1sport.barcodeapp.common.changeButtons
import ru.n1sport.barcodeapp.common.setVisibility
import ru.n1sport.barcodeapp.common.unsafeLazy


class PassportFragment : Fragment() {

    // region injects
    private val viewModel: PassportViewModel by viewModel()
    private val operationsAdapter: OperationsAdapter by inject { parametersOf(::operationUpdate) }
    // endregion

    // region fields
    private val rxPermissions by unsafeLazy {
        RxPermissions(this)
    }
    private val disposeBag = CompositeDisposable()
    // endregion

    // region lifecycle
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_passport, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleNavigation()
        handleDressmaker()
        handlePassportInfo()
        handleOperations()
        handleSend()
        handleCallMechanic()
        handleCallMaster()
        handleExit()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposeBag.clear()
    }
    // endregion

    // region handlers
    private fun handleExit() {
        image_view_exit.clicks()
            .map { PassportIntention.ExitClicked }
            .subscribe(viewModel.intentions)

        viewModel.sideEffects.filter { it === PassportSideEffect.ShowExitAlertDialog }
            .subscribe {
                showExitAlertDialog()
            }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.isExitEnabled }
            .subscribe {
                image_view_exit?.isEnabled = it
            }
            .addTo(disposeBag)
    }

    private fun handleNavigation() {
        button_to_dressmaker_barcode_scanning.clicks()
            .flatMap { requestCameraPermission() }
            .filter { it }
            .map { PassportIntention.NavigateToDressmakerBarcodeScanningClicked }
            .subscribe(viewModel.intentions)

        image_view_to_dressmaker_barcode_scanning.clicks()
            .flatMap { requestCameraPermission() }
            .filter { it }
            .map { PassportIntention.NavigateToDressmakerBarcodeScanningClicked }
            .subscribe(viewModel.intentions)

        button_to_passport_barcode_scanning.clicks()
            .flatMap { requestCameraPermission() }
            .filter { it }
            .map { PassportIntention.NavigateToPassportBarcodeScanningClicked }
            .subscribe(viewModel.intentions)

        viewModel.viewState
            .map { it.isButtonToPassportBarcodeScanningEnabled }
            .subscribe {
                button_to_passport_barcode_scanning?.isEnabled = it
            }
            .addTo(disposeBag)
    }

    private fun handleDressmaker() {
        viewModel.viewState
            .map { it.dressmaker.lastname }
            .filter { text_view_lastname?.text != it }
            .subscribe { text_view_lastname?.text = it }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.dressmaker.firstname }
            .filter { text_view_firstname?.text != it }
            .subscribe { text_view_firstname?.text = it }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.dressmaker.patronymic }
            .filter { text_view_patronymic?.text != it }
            .subscribe { text_view_patronymic?.text = it }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.dressmaker.isButtonToDressmakerBarcodeScanningVisible }
            .subscribe { button_to_dressmaker_barcode_scanning?.setVisibility(it) }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.dressmaker.isDressmakerImgVisible }
            .subscribe { image_view_to_dressmaker_barcode_scanning?.setVisibility(it) }
            .addTo(disposeBag)

        viewModel.viewState
            .filter { it.dressmaker.isDressmakerImgVisible }
            .map { it.dressmaker.imgUrl }
            .distinctUntilChanged()
            .subscribe {
                image_view_to_dressmaker_barcode_scanning?.apply {
                    loadImage(it, this)
                }
            }
            .addTo(disposeBag)
    }

    private fun handlePassportInfo() {
        // design img
        viewModel.viewState
            .filter { it.passport.isDesignImgCanLoaded }
            .map { it.passport.designImgUrl }
            .distinctUntilChanged()
            .subscribe {
                image_view_product_design?.apply {
                    loadImage(it, this)
                }
                image_view_product_design_full_screen?.apply {
                    loadImage(it, this)
                }
            }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.passport.isDesignImgVisible }
            .subscribe { image_view_product_design?.setVisibility(it) }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.passport.isLogoVisible }
            .subscribe { image_view_logo?.setVisibility(it) }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.passport.isFullScreenDesignImgVisible }
            .subscribe { linear_layout_product_design_full_screen?.setVisibility(it) }
            .addTo(disposeBag)

        image_view_product_design.clicks()
            .map { PassportIntention.DesignImageClicked }
            .subscribe(viewModel.intentions)

        linear_layout_product_design_full_screen.clicks()
            .map { PassportIntention.FullScreenDesignImageClicked }
            .subscribe(viewModel.intentions)

        // order
        viewModel.viewState
            .filter { it.passport.order != null }
            .map { it.passport.order }
            .filter { text_view_order?.text != it }
            .subscribe { text_view_order?.text = it }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.passport.isOrderVisible }
            .subscribe { group_order?.setVisibility(it) }
            .addTo(disposeBag)

        // passport
        viewModel.viewState
            .filter { it.passport.passportId != null || it.passport.date != null }
            .map {
                it.passport.passportId +
                    if (it.passport.date.isNullOrEmpty()) ""
                    else " от ${it.passport.date}"
            }
            .filter { text_view_passport?.text != it }
            .subscribe { text_view_passport?.text = it }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.passport.isPassportVisible }
            .subscribe { group_passport?.setVisibility(it) }
            .addTo(disposeBag)

        // count
        viewModel.viewState
            .filter { it.passport.count != null }
            .map { it.passport.count }
            .filter { text_view_count?.text != it.toString() }
            .subscribe { text_view_count?.text = it.toString() }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.passport.isCountVisible }
            .subscribe { group_count?.setVisibility(it) }
            .addTo(disposeBag)

        // product
        viewModel.viewState
            .filter { it.passport.product != null }
            .map { it.passport.product }
            .filter { text_view_product?.text != it }
            .subscribe { text_view_product?.text = it }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.passport.isProductVisible }
            .subscribe { group_product?.setVisibility(it) }
            .addTo(disposeBag)

        // option
        viewModel.viewState
            .filter { it.passport.option != null }
            .map { it.passport.option }
            .filter { text_view_option?.text != it }
            .subscribe { text_view_option?.text = it }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.passport.isOptionVisible }
            .subscribe { group_option?.setVisibility(it) }
            .addTo(disposeBag)

        // design
        viewModel.viewState
            .filter { it.passport.design != null }
            .map { it.passport.design }
            .filter { text_view_design?.text != it }
            .subscribe { text_view_design?.text = it }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.passport.isDesignVisible }
            .subscribe { group_design?.setVisibility(it) }
            .addTo(disposeBag)

//        TODO(): Раскоментировать для показа orderId
//        // orderId
//        viewModel.viewState
//            .filter { it.passport.orderId != null }
//            .map { it.passport.orderId }
//            .filter { text_view_order_id?.text != it }
//            .subscribe { text_view_order_id?.text = it }
//            .addTo(disposeBag)
//
//        viewModel.viewState
//            .map { it.passport.isOrderIdVisible }
//            .subscribe { group_order_id?.setVisibility(it) }
//            .addTo(disposeBag)

//        TODO(): Раскоментировать для показа customer
//        // customer
//        viewModel.viewState
//            .filter { it.passport.customer != null }
//            .map { it.passport.customer }
//            .filter { text_view_customer?.text != it }
//            .subscribe { text_view_customer?.text = it }
//            .addTo(disposeBag)
//
//        viewModel.viewState
//            .map { it.passport.isCustomerVisible }
//            .subscribe { group_customer?.setVisibility(it) }
//            .addTo(disposeBag)
    }

    private fun handleOperations() {
        recycler_view_operations?.apply {
            setHasFixedSize(true)
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = operationsAdapter
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false

            addItemDecoration(
                DividerItemDecoration(
                    context,
                    DividerItemDecoration.VERTICAL
                )
            )
        }

        viewModel.viewState
            .map { it.passport.operations }
            .distinctUntilChanged()
            .subscribe { newOperations ->
                val productDiffUtilCallback =
                    OperationStateDiffUtilCallback(operationsAdapter.operations, newOperations)
                val productDiffResult = DiffUtil.calculateDiff(productDiffUtilCallback)

                operationsAdapter.operations = newOperations
                productDiffResult.dispatchUpdatesTo(operationsAdapter)
            }
            .addTo(disposeBag)

        viewModel.viewState
            .map { it.passport.isOperationVisible }
            .subscribe {
                linear_layout_operations_container.isVisible = it
            }
            .addTo(disposeBag)
    }

    private fun handleSend() {
        viewModel.viewState
            .map { it.isSendButtonEnabled }
            .subscribe {
                button_send_work?.isEnabled = it
            }
            .addTo(disposeBag)

        button_send_work.clicks()
            .map { PassportIntention.SendOperationsClicked }
            .subscribe(viewModel.intentions)

        viewModel.sideEffects.filter { it === PassportSideEffect.ShowSendOperationsAlertDialog }
            .subscribe {
                showSendOperationsAlertDialog()
            }
            .addTo(disposeBag)
    }

    private fun handleCallMechanic() {
        viewModel.viewState
            .map { it.isCallMechanicEnabled }
            .subscribe {
                button_call_mechanic?.isEnabled = it
            }
            .addTo(disposeBag)

        button_call_mechanic.clicks()
            .map { PassportIntention.CallMechanicClicked }
            .subscribe(viewModel.intentions)

        viewModel.sideEffects.filter { it === PassportSideEffect.ShowCallMechanicAlertDialog }
            .subscribe {
                showCallMechanicAlertDialog()
            }
            .addTo(disposeBag)
    }

    private fun handleCallMaster() {
        viewModel.viewState
            .map { it.isCallMasterEnabled }
            .subscribe {
                button_call_master?.isEnabled = it
            }
            .addTo(disposeBag)

        button_call_master.clicks()
            .map { PassportIntention.CallMasterClicked }
            .subscribe(viewModel.intentions)

        viewModel.sideEffects.filter { it === PassportSideEffect.ShowCallMasterAlertDialog }
            .subscribe {
                showCallMasterAlertDialog()
            }
            .addTo(disposeBag)
    }
    // endregion

    private fun requestCameraPermission() = rxPermissions
        .request(
            Manifest.permission.CAMERA
        )
        .doOnNext {
            if (!it) {
                AlertDialog.Builder(requireContext()).apply {
                    setTitle("Разрешение")
                    setMessage(
                        "Для сканирования штрихкода нужно разрешение на доступ к камере."
                    )
                    setCancelable(false)
                    setPositiveButton("Ок") { dialog, _ ->
                        dialog.cancel()
                    }
                }
                    .create()
                    .show()
            }
        }

    private fun operationUpdate(newOperationState: OperationState) {
        viewModel.intentions.onNext(PassportIntention.OperationUpdated(newOperationState))
    }

    private fun loadImage(imgUrl: String?, target: ImageView) {
        Glide.with(this)
            .load(imgUrl)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .placeholder(R.drawable.no_image)
            .into(target)
    }

    private fun showExitAlertDialog() {
        val title = SpannableString("Выйти?").apply {
            setSpan(RelativeSizeSpan(2f), 0, length, 0)
        }
        val onPositiveButtonClick = {
            viewModel.intentions.onNext(PassportIntention.ExitConfirmed)
        }

        showConfirmOperationsAlertDialog(title, onPositiveButtonClick)
    }

    private fun showCallMasterAlertDialog() {
        val title = SpannableString("Вызвать бригадира?").apply {
            setSpan(RelativeSizeSpan(2f), 0, length, 0)
        }
        val onPositiveButtonClick = {
            viewModel.intentions.onNext(PassportIntention.CallMasterConfirmed)
        }

        showConfirmOperationsAlertDialog(title, onPositiveButtonClick)
    }

    private fun showCallMechanicAlertDialog() {
        val title = SpannableString("Вызвать механика?").apply {
            setSpan(RelativeSizeSpan(2f), 0, length, 0)
        }
        val onPositiveButtonClick = {
            viewModel.intentions.onNext(PassportIntention.CallMechanicConfirmed)
        }

        showConfirmOperationsAlertDialog(title, onPositiveButtonClick)
    }

    private fun showSendOperationsAlertDialog() {
        val title = SpannableString("Отправить операции?").apply {
            setSpan(RelativeSizeSpan(2f), 0, length, 0)
        }
        val onPositiveButtonClick = {
            viewModel.intentions.onNext(PassportIntention.SendOperationsConfirmed)
        }

        showConfirmOperationsAlertDialog(title, onPositiveButtonClick)
    }

    private fun showConfirmOperationsAlertDialog(
        title: CharSequence,
        onPositiveButtonClick: () -> Unit
    ) {
        context?.alert {
            setTitle(title)
            setCancelable(false)
            setPositiveButton("Да") { _, _ ->
                onPositiveButtonClick()
            }
            setNegativeButton("Отмена") { _, _ ->

            }
        }
            ?.apply {
                show()
                changeButtons()
            }
    }
}