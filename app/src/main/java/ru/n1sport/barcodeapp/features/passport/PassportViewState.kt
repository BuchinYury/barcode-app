package ru.n1sport.barcodeapp.features.passport


data class PassportViewState(
    val dressmaker: DressmakerState,
    val passport: PassportState = PassportState()
) {
    val isSendButtonEnabled = dressmaker.barcodeDetected &&
        passport.barcodeDetected &&
        passport.operations.any { it.isAdded }
    val isCallMechanicEnabled = dressmaker.barcodeDetected
    val isCallMasterEnabled = dressmaker.barcodeDetected
    val isExitEnabled = dressmaker.barcodeDetected

    val isButtonToPassportBarcodeScanningEnabled = dressmaker.barcodeDetected
}

data class DressmakerState(
    val barcodeDetected: Boolean = false,
    val lastname: String,
    val firstname: String,
    val patronymic: String,
    val imgUrl: String? = null
) {
    val isButtonToDressmakerBarcodeScanningVisible = imgUrl.isNullOrEmpty()
    val isDressmakerImgVisible = !imgUrl.isNullOrEmpty()
}

data class PassportState(
    val barcodeDetected: Boolean = false,
    val orderId: String? = null,
    val order: String? = null,
    val customer: String? = null,
    val count: Int? = null,
    val product: String? = null,
    val option: String? = null,
    val design: String? = null,
    val passportId: String? = null,
    val date: String? = null,
    val designImgUrl: String? = null,
    val operations: List<OperationState> = emptyList(),
    val isFullScreenDesignImgVisible: Boolean = false
) {
    val isDesignImgVisible = designImgUrl != null
    val isDesignImgCanLoaded = !designImgUrl.isNullOrEmpty()
    val isLogoVisible = !barcodeDetected
    val isOrderIdVisible = barcodeDetected
    val isOrderVisible = barcodeDetected
    val isCustomerVisible = barcodeDetected
    val isCountVisible = barcodeDetected
    val isProductVisible = barcodeDetected
    val isOptionVisible = barcodeDetected
    val isDesignVisible = barcodeDetected
    val isPassportVisible = barcodeDetected
    val isOperationVisible = operations.isNotEmpty()
}

data class OperationState(
    val id: String,
    val sku: String,
    val name: String,
    val count: Int,
    val madeCount: Int = count,
    val isAdded: Boolean = false
) {
    fun isNeedUpdate(other: OperationState): Boolean =
        areItemTheSame(other) && areContentTheSame(other)

    private fun areItemTheSame(other: OperationState): Boolean =
        this.id == other.id && this.name == other.name

    private fun areContentTheSame(other: OperationState): Boolean =
        this.isAdded != other.isAdded || this.madeCount != other.madeCount || this.count != other.count
}