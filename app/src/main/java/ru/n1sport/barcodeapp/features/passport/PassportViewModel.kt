package ru.n1sport.barcodeapp.features.passport

import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import ru.n1sport.barcodeapp.common.DressmakerBarcodeScannerScreen
import ru.n1sport.barcodeapp.common.PassportBarcodeScannerScreen
import ru.n1sport.bll.stores.AppStore
import ru.n1sport.bll.stores.AppStoreAction
import ru.n1sport.bll.stores.Operation
import ru.terrakok.cicerone.Router


sealed class PassportIntention {
    object NavigateToDressmakerBarcodeScanningClicked : PassportIntention()
    object NavigateToPassportBarcodeScanningClicked : PassportIntention()
    object ExitClicked : PassportIntention()
    object ExitConfirmed : PassportIntention()
    object SendOperationsClicked : PassportIntention()
    object SendOperationsConfirmed : PassportIntention()
    object CallMechanicClicked : PassportIntention()
    object CallMechanicConfirmed : PassportIntention()
    object CallMasterClicked : PassportIntention()
    object CallMasterConfirmed : PassportIntention()
    object DesignImageClicked : PassportIntention()
    object FullScreenDesignImageClicked : PassportIntention()
    class OperationUpdated(val newOperationState: OperationState) : PassportIntention()
}

sealed class PassportSideEffect {
    object ShowCallMechanicAlertDialog : PassportSideEffect()
    object CallMechanic : PassportSideEffect()
    object ShowCallMasterAlertDialog : PassportSideEffect()
    object CallMaster : PassportSideEffect()
    object ShowExitAlertDialog : PassportSideEffect()
    object Exit : PassportSideEffect()
    object NavigateToDressmakerBarcodeScanning : PassportSideEffect()
    object NavigateToPassportBarcodeScanning : PassportSideEffect()
    object ShowSendOperationsAlertDialog : PassportSideEffect()
    class SendOperations(val operations: List<ru.n1sport.bll.models.Operation>) : PassportSideEffect()
}


class PassportViewModel(
    initialState: PassportViewState,
    router: Router,
    appStore: AppStore
) : ViewModel() {
    val intentions: Subject<PassportIntention> = PublishSubject.create()
    val sideEffects: Subject<PassportSideEffect> = PublishSubject.create()
    val viewState: Subject<PassportViewState> = BehaviorSubject.create()

    private val disposeBag = CompositeDisposable()

    init {
        val dressmaker = appStore.state
            .map { it.dressmaker }
            .distinctUntilChanged()
            .map { dressmakerState ->
                { currentState: PassportViewState ->
                    val newDressmakerState = currentState.dressmaker.copy(
                        barcodeDetected = !dressmakerState.barcode.isNullOrEmpty(),
                        lastname = dressmakerState.lastname ?: "Фамилия",
                        firstname = dressmakerState.firstname ?: "Имя",
                        patronymic = dressmakerState.patronymic ?: "Отчество",
                        imgUrl = dressmakerState.imgUrl
                    )
                    currentState.copy(
                        dressmaker = newDressmakerState
                    )
                }
            }
        val passport = appStore.state
            .map { it.passport }
            .map { passport ->
                { currentState: PassportViewState ->
                    val newPassportState = currentState.passport.copy(
                        barcodeDetected = !passport.barcode.isNullOrEmpty(),
                        orderId = passport.orderId,
                        order = passport.order,
                        customer = passport.customer,
                        count = passport.count,
                        product = passport.product,
                        option = passport.option,
                        design = passport.design,
                        designImgUrl = passport.designImgUrl,
                        passportId = passport.passportId,
                        date = passport.date,
                        operations = passport.operations.map {
                            OperationState(
                                id = it.id,
                                sku = it.sku,
                                name = it.name,
                                count = it.count
                            )
                        }
                    )
                    currentState.copy(
                        passport = newPassportState
                    )
                }
            }
        val operationUpdated = intentions.filter { it is PassportIntention.OperationUpdated }
            .map { (it as PassportIntention.OperationUpdated).newOperationState }
            .map { newOperationState ->
                { currentState: PassportViewState ->
                    currentState.copy(
                        passport = currentState.passport.copy(
                            operations = currentState.passport.operations.map {
                                if (newOperationState.id == it.id && newOperationState.name == it.name)
                                    newOperationState
                                else
                                    it
                            }
                        )
                    )
                }
            }
        val designImageClicked = intentions.filter { it === PassportIntention.DesignImageClicked }
            .map {
                { currentState: PassportViewState ->
                    currentState.copy(
                        passport = currentState.passport.copy(
                            isFullScreenDesignImgVisible = true
                        )
                    )
                }
            }
        val fullScreenDesignImageClicked = intentions.filter { it === PassportIntention.FullScreenDesignImageClicked }
            .map {
                { currentState: PassportViewState ->
                    currentState.copy(
                        passport = currentState.passport.copy(
                            isFullScreenDesignImgVisible = false
                        )
                    )
                }
            }

        val stateReducers = listOf(
            dressmaker,
            passport,
            operationUpdated,
            designImageClicked,
            fullScreenDesignImageClicked
        )

        Observable.merge(stateReducers)
            .scan(initialState) { currentState, stateReducer -> stateReducer(currentState) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(viewState)
    }

    init {
        intentions.filter { it === PassportIntention.NavigateToDressmakerBarcodeScanningClicked }
            .map { PassportSideEffect.NavigateToDressmakerBarcodeScanning }
            .subscribe(sideEffects)

        sideEffects.filter { it === PassportSideEffect.NavigateToDressmakerBarcodeScanning }
            .subscribe {
                router.navigateTo(DressmakerBarcodeScannerScreen)
            }
            .addTo(disposeBag)

        intentions.filter { it === PassportIntention.NavigateToPassportBarcodeScanningClicked }
            .map { PassportSideEffect.NavigateToPassportBarcodeScanning }
            .subscribe(sideEffects)

        sideEffects.filter { it === PassportSideEffect.NavigateToPassportBarcodeScanning }
            .subscribe {
                router.navigateTo(PassportBarcodeScannerScreen)
            }
            .addTo(disposeBag)
    }

    init {
        intentions.filter { it === PassportIntention.ExitClicked }
            .map { PassportSideEffect.ShowExitAlertDialog }
            .subscribe(sideEffects)

        intentions.filter { it === PassportIntention.ExitConfirmed }
            .map { PassportSideEffect.Exit }
            .subscribe(sideEffects)

        sideEffects.filter { it === PassportSideEffect.Exit }
            .map { AppStoreAction.Exit }
            .subscribe(appStore.actions)

    }

    init {
        intentions.filter { it === PassportIntention.CallMechanicClicked }
            .map { PassportSideEffect.ShowCallMechanicAlertDialog }
            .subscribe(sideEffects)

        intentions.filter { it === PassportIntention.CallMechanicConfirmed }
            .map { PassportSideEffect.CallMechanic }
            .subscribe(sideEffects)

        sideEffects.filter { it === PassportSideEffect.CallMechanic }
            .map { AppStoreAction.CallMechanic }
            .subscribe(appStore.actions)
    }

    init {
        intentions.filter { it === PassportIntention.CallMasterClicked }
            .map { PassportSideEffect.ShowCallMasterAlertDialog }
            .subscribe(sideEffects)

        intentions.filter { it === PassportIntention.CallMasterConfirmed }
            .map { PassportSideEffect.CallMaster }
            .subscribe(sideEffects)

        sideEffects.filter { it === PassportSideEffect.CallMaster }
            .map { AppStoreAction.CallMaster }
            .subscribe(appStore.actions)
    }

    init {
        intentions.filter { it === PassportIntention.SendOperationsClicked }
            .map { PassportSideEffect.ShowSendOperationsAlertDialog }
            .subscribe(sideEffects)

        intentions.filter { it === PassportIntention.SendOperationsConfirmed }
            .withLatestFrom(viewState) { _, currentViewState ->
                currentViewState.passport.operations
                    .filter { it.madeCount != 0 && it.isAdded }
                    .map {
                        ru.n1sport.bll.models.Operation(
                            it.id,
                            it.madeCount
                        )
                    }
            }
            .map { PassportSideEffect.SendOperations(it) }
            .subscribe(sideEffects)

        sideEffects.filter { it is PassportSideEffect.SendOperations }
            .map { (it as PassportSideEffect.SendOperations).operations }
            .map { AppStoreAction.SendOperations(it) }
            .subscribe(appStore.actions)
    }

    override fun onCleared() {
        super.onCleared()
        disposeBag.clear()
    }
}

