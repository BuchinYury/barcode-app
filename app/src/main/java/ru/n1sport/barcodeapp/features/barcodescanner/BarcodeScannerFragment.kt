package ru.n1sport.barcodeapp.features.barcodescanner

import android.graphics.BitmapFactory
import android.graphics.ImageFormat
import android.graphics.Rect
import android.graphics.YuvImage
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.jakewharton.rxbinding3.view.clicks
import io.fotoapparat.Fotoapparat
import io.fotoapparat.configuration.UpdateConfiguration
import io.fotoapparat.preview.Frame
import io.fotoapparat.preview.FrameProcessor
import io.fotoapparat.selector.torch
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_barcode_scanner.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import ru.n1sport.barcodeapp.R
import ru.n1sport.barcodeapp.common.decodeBitmap
import ru.n1sport.barcodeapp.common.unsafeLazy
import java.io.ByteArrayOutputStream
import java.util.concurrent.locks.ReentrantLock

enum class BarcodeSource {
    DressmakerBarcode,
    PassportBarcode;
}

class BarcodeScannerFragment : Fragment() {

    companion object {
        private const val ARG_BARCODE_SOURCE = "ARG_BARCODE_SOURCE"

        fun getInstance(barcodeSource: BarcodeSource) = BarcodeScannerFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARG_BARCODE_SOURCE, barcodeSource)
            }
        }
    }

    // region injects
    private val viewModel: BarcodeScannerViewModel by viewModel { parametersOf(barcodeSource) }
    private val fotoapparat: Fotoapparat by inject {
        parametersOf(
            requireContext(),
            camera_view_barcode_scanner,
            frameProcessor
        )
    }
    // endregion

    // region arguments
    private val barcodeSource by unsafeLazy {
        arguments?.getSerializable(ARG_BARCODE_SOURCE)
            ?: throw IllegalAccessException("barcodeSource должен присутствовать в аргументах")
    }
    // endregion

    // region fields
    private val frameProcessor = object : FrameProcessor {
        private val lock = ReentrantLock()
        private var isIntentionsSend = false

        override fun process(frame: Frame) {
            val width = frame.size.width
            val height = frame.size.height

            val baos = ByteArrayOutputStream().apply {
                YuvImage(frame.image, ImageFormat.NV21, width, height, null)
                    .compressToJpeg(Rect(0, 0, width, height), 100, this)
            }
            val jpegByteArray = baos.toByteArray()
            baos.close()

            val bitmap = BitmapFactory.decodeByteArray(jpegByteArray, 0, jpegByteArray.size)

            val barcode = decodeBitmap(bitmap)

            lock.lock()
            if (barcode != null && !isIntentionsSend) {
                viewModel.intentions.onNext(BarcodeScannerIntention.BarcodeDetected(barcode))
                isIntentionsSend = true
            }
            try {
                Thread.sleep(100)
            } catch (_: Throwable) {

            }
            lock.unlock()
        }
    }
    private val disposeBag = CompositeDisposable()
    // endregion

    // region lifecycle
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_barcode_scanner, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleNavigation()
        userInvite()
    }

    override fun onResume() {
        super.onResume()
        fotoapparat.start()
        handleFlash()
    }

    override fun onPause() {
        super.onPause()
        fotoapparat.stop()
    }
    // endregion

    // region handlers
    private fun handleNavigation() {
        image_view_back_button.clicks()
            .map { BarcodeScannerIntention.NavigateToBackClicked }
            .subscribe(viewModel.intentions)
    }

    private fun handleFlash() {
        fotoapparat.updateConfiguration(UpdateConfiguration(flashMode = torch()))
    }

    private fun userInvite() {
        viewModel.viewState
            .map { it.inviteText }
            .subscribe {
                text_view_user_invite?.text = it
            }
            .addTo(disposeBag)
    }
    // endregion
}