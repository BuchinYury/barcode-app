package ru.n1sport.dal

import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.n1sport.dal.gateways.BasicAuthInterceptor
import ru.n1sport.dal.gateways.N1SportApi
import ru.n1sport.dal.gateways.SharedPreferencesGateway


val gatewayModule = module {
    single { SharedPreferencesGateway(get()) }
    single {
        OkHttpClient.Builder()
            .addInterceptor(BasicAuthInterceptor("test", "test"))
            .addInterceptor(ChuckInterceptor(androidContext()))
            .build()
    }
    single {
        val client: OkHttpClient = get()

        Retrofit.Builder()
            .baseUrl(BuildConfig.DEFAULT_SERVER_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
            .build()
            .create(N1SportApi::class.java)
    }
}