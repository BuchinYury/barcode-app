package ru.n1sport.dal.gateways

import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.PUT
import ru.n1sport.dal.BuildConfig
import ru.n1sport.dal.models.request.CallMasterRequest
import ru.n1sport.dal.models.request.CallMechanicRequest
import ru.n1sport.dal.models.request.DressmakerRequest
import ru.n1sport.dal.models.request.PassportRequest
import ru.n1sport.dal.models.request.SendOperationsRequest
import ru.n1sport.dal.models.response.DressmakerResponse
import ru.n1sport.dal.models.response.PassportResponse

interface N1SportApi {
    @POST("futbitex/hs/futbitexservice/name")
    fun getDressmaker(
        @Body body: DressmakerRequest
    ): Single<DressmakerResponse>

    @POST("futbitex/hs/futbitexservice/passport")
    fun getPassport(
        @Body body: PassportRequest
    ): Single<PassportResponse>

    @PUT("futbitex/hs/futbitexservice/passport")
    fun sendOperations(
        @Body body: SendOperationsRequest
    ): Completable

    @POST("futbitex/hs/futbitexservice/call_mechanic")
    fun callMechanic(
        @Body body: CallMechanicRequest
    ): Completable

    @POST("futbitex/hs/futbitexservice/call_master")
    fun callMaster(
        @Body body: CallMasterRequest
    ): Completable
}

class BasicAuthInterceptor(
    login: String,
    password: String
) : Interceptor {
    private val credentials = Credentials.basic(login, password)

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val authenticatedRequest = request.newBuilder()
            .header("Authorization", credentials).build()
        return chain.proceed(authenticatedRequest)
    }
}