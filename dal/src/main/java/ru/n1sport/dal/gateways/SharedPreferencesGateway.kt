package ru.n1sport.dal.gateways

import android.content.SharedPreferences
import androidx.core.content.edit
import ru.n1sport.dal.BuildConfig

private const val PREFKEY_SERVER_URL = "prefkey:server_url"

class SharedPreferencesGateway(
    private val sharedPreferences: SharedPreferences
) {
    var serverUrl: String
        get() = sharedPreferences.getString(PREFKEY_SERVER_URL, BuildConfig.DEFAULT_SERVER_URL)
            ?: BuildConfig.DEFAULT_SERVER_URL
        set(value) {
            sharedPreferences.edit(commit = true) {
                putString(PREFKEY_SERVER_URL, value)
            }
        }
}