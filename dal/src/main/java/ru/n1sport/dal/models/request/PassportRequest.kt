package ru.n1sport.dal.models.request

import com.google.gson.annotations.SerializedName

class PassportRequest(
    @SerializedName("id_pass") val passportId: String,
    @SerializedName("id_empl") val dressmakerId: String
)