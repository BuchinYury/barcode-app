package ru.n1sport.dal.models.request

import com.google.gson.annotations.SerializedName


class SendOperationsRequest(
    @SerializedName("id_pass") val passportId: String,
    @SerializedName("id_empl") val dressmakerId: String,
    @SerializedName("operations") val madeOperations: List<Operation>
)

class Operation(
    val id: String,
    @SerializedName("count") val madeCount: Int
)