package ru.n1sport.dal.models.request

import com.google.gson.annotations.SerializedName


class CallMechanicRequest(
    @SerializedName("id_empl") val dressmakerId: String
)