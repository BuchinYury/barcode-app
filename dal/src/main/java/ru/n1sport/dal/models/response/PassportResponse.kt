package ru.n1sport.dal.models.response

import com.google.gson.annotations.SerializedName

class PassportResponse(
    val order: String,
    @SerializedName("order_id") val orderId: String,
    val customer: String,
    val product: String,
    val option: String,
    val design: String,
    @SerializedName("design_img_url") val designImgUrl: String,
    @SerializedName("id_passport") val passpotrId: String,
    @SerializedName("id_date") val date: String,
    val count: Int,
    val operations: List<Operation>
)

class Operation(
    val id: String,
    val sku: String,
    val name: String,
    val count: Int
)
