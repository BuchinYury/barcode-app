package ru.n1sport.dal.models.response

import com.google.gson.annotations.SerializedName

class DressmakerResponse(
    val lastname: String,
    val firstname: String,
    val patronymic: String,
    @SerializedName("img_url") val imgUrl: String
)